import { Grid, GridProps, Link, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import cls from 'classnames'

import HTB from '../HTBSection/assets/htbLogo.svg'

import Bunnycdn from './assets/bunnycdn.svg'
import Caido from './assets/caido.svg'
/* import Linode from './assets/linode.svg' */

const useStyles = makeStyles(theme => ({
  trustLogosBlock: {
    marginTop: theme.spacing(2)
  },
  img: {
    width: '100%',
    height: '100%',
    maxHeight: theme.spacing(6),
    margin: 'auto',
    fill: theme.palette.mode === 'dark' ? 'white' : 'inherit',
    '& #linode-block': {
      fill: '#03232E'
    },
    '& #linode-text': {
      fill: theme.palette.mode === 'light' ? '#03232E' : '#FFF'
    },
    '& #bunny-text': {
      fill: theme.palette.mode === 'light' ? '#03232E' : '#FFF'
    }
  },
  caido: {
    maxHeight: theme.spacing(8)
  }
}))

const TrustSection = (props: GridProps) => {
  const classes = useStyles()
  return (
    <Grid {...props} container justifyContent="center" alignItems="center" item xs={10}>
      <Grid item xs={10}>
        <Typography variant="body1" align="center" style={{ opacity: 1 }}>
          In partnership with
        </Typography>
      </Grid>
      <Grid
        className={classes.trustLogosBlock}
        container
        item
        xs={10}
        md={8}
        spacing={4}
        justifyContent="center"
      >
        <Grid item xs={12} md={8} lg={4}>
          <Link href="https://hacktheboxltd.sjv.io/9gXmE5">
            <HTB className={classes.img} />
          </Link>
        </Grid>
        <Grid item xs={12} md={8} lg={4}>
          <Link href="https://bunny.net?ref=ppalfbefw3">
            <Bunnycdn className={classes.img} />
          </Link>
        </Grid>
        <Grid item xs={12} md={8}>
          <Link href="https://caido.io/">
            <Caido className={cls(classes.img, classes.caido)} />
          </Link>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default TrustSection
