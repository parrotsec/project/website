import { CardActionArea, Grid, Paper, Stack, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import cls from 'classnames'

import BunnyLogo from 'containers/HomeContainers/TrustSection/assets/bunnycdn.svg'
import CaidoLogo from 'containers/HomeContainers/TrustSection/assets/caido.svg'

const useStyles = makeStyles(theme => ({
  actionArea: {
    display: 'flex',
    borderRadius: 24,
    marginTop: theme.spacing(2),
    height: '100%'
  },
  sponsorPaper: {
    height: '100%',
    padding: theme.spacing(4)
  },
  sponsorImg: {
    width: '100%',
    backgroundSize: 'cover',
    display: 'block',
    margin: 'auto',
    fill: theme.palette.mode === 'dark' ? 'white' : 'inherit',
    '& #linode-block': {
      fill: '#03232E'
    },
    '& #linode-text': {
      fill: theme.palette.mode === 'light' ? '#03232E' : '#FFF'
    },
    '& #bunny-text': {
      fill: theme.palette.mode === 'light' ? '#03232E' : '#FFF'
    }
  },
  caido: {
    marginTop: theme.spacing(2)
  },
  caidoImg: {
    maxHeight: theme.spacing(16)
  }
}))

const SponsorSection = () => {
  const classes = useStyles()

  return (
    <Grid container item xs={12} md={9}>
      {/* BunnyCDN */}
      <Grid container item xs={12} alignItems="center" justifyContent="center" wrap="nowrap">
        <Stack direction="row" spacing={2}>
          <Grid item xs={12} justifyContent="center">
            <CardActionArea className={classes.actionArea} href="https://bunny.net?ref=ppalfbefw3">
              <Paper className={classes.sponsorPaper} elevation={0}>
                <Typography variant="h4" gutterBottom>
                  BunnyCDN
                </Typography>
                <Stack direction="row" spacing={2}>
                  <Typography variant="subtitle2">
                    BunnyCDN is a private and fast content distribution network used to boost the
                    download of the Parrot system. Go faster than the fastest with the
                    next-generation CDN, edge storage, and optimization service.
                  </Typography>
                  <BunnyLogo className={classes.sponsorImg} />
                </Stack>
              </Paper>
            </CardActionArea>
          </Grid>
        </Stack>
      </Grid>
      {/* Caido */}
      <Grid
        container
        item
        xs={12}
        alignItems="center"
        justifyContent="center"
        wrap="nowrap"
        className={classes.caido}
      >
        <Stack direction="row" spacing={2}>
          <Grid item xs={12} justifyContent="center">
            <CardActionArea className={classes.actionArea} href="https://caido.io/">
              <Paper className={classes.sponsorPaper} elevation={0}>
                <Typography variant="h4" gutterBottom>
                  Caido
                </Typography>
                <Stack direction="row" spacing={2}>
                  <Typography variant="subtitle2">
                    A lightweight web security auditing toolkit. Caido aims to help security
                    professionals and enthusiasts audit web applications with efficiency and ease.
                  </Typography>
                  <CaidoLogo className={cls(classes.sponsorImg, classes.caidoImg)} />
                </Stack>
              </Paper>
            </CardActionArea>
          </Grid>
        </Stack>
      </Grid>
    </Grid>
  )
}

export default SponsorSection
