import { Link, Typography, Grid, useMediaQuery, Theme } from '@mui/material'
import { makeStyles, createStyles } from '@mui/styles'
import { Stack, useTheme } from '@mui/system'

import OSCard from 'components/OSCard'
import Debian from 'containers/HomeContainers/OSSection/assets/debian.svg'
import Docker from 'containers/HomeContainers/OSSection/assets/docker.svg'
import WSL from 'containers/HomeContainers/OSSection/assets/wsl.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    debianIcon: {
      background: 'linear-gradient(99deg, #00FF87 0%, rgba(61,203,34,1) 0%, rgba(0,204,155,1) 100%)'
    },
    dockerIcon: {
      background:
        'linear-gradient(99deg, rgba(29,3,130,0.8169861694677871) 0%, rgba(0,91,204,1) 0%)'
    },
    wslIcon: {
      background: 'linear-gradient(90deg, #FFFFFF 58%, #EAEAEA 99%)'
    },
    headingSubTitle: {
      marginBottom: theme.spacing(5)
    }
  })
)

export const AlternativeDownloadBlock = () => {
  const classes = useStyles()
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'))
  return (
    <>
      <Typography className={classes.headingSubTitle} variant="h3" align="center">
        Alternative installation methods
      </Typography>

      <Grid item xs={12} md={9}>
        <Stack direction={isMobile ? 'column' : 'row'} spacing={4}>
          <OSCard
            Icon={Docker}
            iconClassName={classes.dockerIcon}
            title="Docker"
            link="https://parrot.run"
            isLinkCard
            buttonText="Link"
          >
            Pre-packaged Docker image of the Parrot operating system. Core, Home and Security
            editions available.
          </OSCard>
          <OSCard
            Icon={Debian}
            iconClassName={classes.debianIcon}
            title="Debian Conversion Script"
            link="https://gitlab.com/parrotsec/project/debian-conversion-script"
            isLinkCard
            buttonText="Link"
          >
            Quick script to convert an existing Debian installation to Parrot (all editions).
          </OSCard>
          <OSCard
            Icon={WSL}
            iconClassName={classes.wslIcon}
            title="WSL"
            link="https://gitlab.com/parrotsec/project/wsl/-/jobs/8932804336/artifacts/download?file_type=archive"
            isLinkCard
            buttonText="Download v0.0.8"
          >
            Feel the full power of our operating system running under Windows!{' '}
            <Link href="https://parrotsec.org/docs/installation/install-with-wsl" underline="none">
              Check out the documentation to learn more
            </Link>
            .
          </OSCard>
        </Stack>
      </Grid>
    </>
  )
}
