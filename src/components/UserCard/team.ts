export default [
  {
    name: 'Lorenzo Faletra',
    nickname: 'palinuro',
    role: 'Team Leader, Core Dev, Infrastructure Manager, Release Manager',
    github: 'https://github.com/PalinuroSec',
    twitter: 'https://twitter.com/palinurosec',
    linkedIn: 'https://linkedin.com/in/lorenzofaletra',
    email: 'mailto:palinuro@parrotsec.org'
  },
  {
    name: 'Nikos Fountas',
    nickname: 'nfou',
    role: 'SVP Operations at Hack The Box, Director and Strategic Advisor at Parrot Sec',
    github: '',
    twitter: '',
    linkedIn: 'https://www.linkedin.com/in/nfou/',
    email: ''
  },
  {
    name: 'Emmanouil Gavriil',
    nickname: 'arkanoid',
    role: 'VP Content at Hack The Box and Director and Strategic Advisor at Parrot Sec',
    github: '',
    twitter: 'https://twitter.com/manosgavriil/',
    linkedIn: 'https://www.linkedin.com/in/emmanouilgavriil/',
    email: ''
  },
  {
    name: 'Dario Camonita',
    nickname: 'danterolle',
    role: 'Senior Systems Engineer',
    github: 'https://github.com/danterolle',
    twitter: 'http://twitter.com/danterolle_',
    linkedIn: 'https://linkedin.com/in/dario-c-74a887149',
    email: 'mailto:danterolle@parrotsec.org'
  },
  {
    name: 'Giulia M. Stabile',
    nickname: 'sh4rk',
    role: 'Director - Marketing & Operations Manager',
    github: '',
    twitter: 'http://twitter.com/giuliamstabile',
    linkedIn: 'https://www.linkedin.com/in/giulia-michelle-stabile-9320a6252/',
    email: 'mailto:sh4rk@parrotsec.org'
  }
]
