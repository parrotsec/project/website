import { ArrowBack } from '@mui/icons-material'
import DeveloperBoardIcon from '@mui/icons-material/DeveloperBoard'
import MemoryIcon from '@mui/icons-material/Memory'
import {
  Stepper,
  Step,
  StepLabel,
  Typography,
  Grid,
  Stack,
  Theme,
  Link,
  useMediaQuery,
  useTheme
} from '@mui/material'
import { makeStyles, createStyles } from '@mui/styles'
import { StaticImageData } from 'next/image'
import { useState } from 'react'

import OSCard from 'components/OSCard'
import PButton from 'components/PButton'
import DESection from 'containers/DownloadContainers/DESection'
import parrotHome1 from 'containers/DownloadContainers/OSHome/assets/parrot-home-1.png'
import parrotHome2 from 'containers/DownloadContainers/OSHome/assets/parrot-home-2.png'
import parrotHome3 from 'containers/DownloadContainers/OSHome/assets/parrot-home-3.png'
import parrotHome4 from 'containers/DownloadContainers/OSHome/assets/parrot-home-4.png'
import parrotHome5 from 'containers/DownloadContainers/OSHome/assets/parrot-home-5.png'
// import Architect from 'containers/HomeContainers/OSSection/assets/Architect.svg'
import htb1 from 'containers/DownloadContainers/OSPwnbox/assets/htb-1.png'
import htb2 from 'containers/DownloadContainers/OSPwnbox/assets/htb-2.png'
import htb3 from 'containers/DownloadContainers/OSPwnbox/assets/htb-3.png'
import htb4 from 'containers/DownloadContainers/OSPwnbox/assets/htb-4.png'
import htb5 from 'containers/DownloadContainers/OSPwnbox/assets/htb-5.png'
import rpi1 from 'containers/DownloadContainers/OSRaspberry/assets/rpi-1.png'
import rpi2 from 'containers/DownloadContainers/OSRaspberry/assets/rpi-2.png'
import parrotSecurity1 from 'containers/DownloadContainers/OSSecurity/assets/parrot-security-1.png'
import parrotSecurity2 from 'containers/DownloadContainers/OSSecurity/assets/parrot-security-2.png'
import parrotSecurity3 from 'containers/DownloadContainers/OSSecurity/assets/parrot-security-3.png'
import parrotSecurity4 from 'containers/DownloadContainers/OSSecurity/assets/parrot-security-4.png'
import parrotSecurity5 from 'containers/DownloadContainers/OSSecurity/assets/parrot-security-5.png'
import HTB from 'containers/HomeContainers/OSSection/assets/hackthebox.svg'
import Home from 'containers/HomeContainers/OSSection/assets/Home.svg'
import IoT from 'containers/HomeContainers/OSSection/assets/iot.svg'
import Live from 'containers/HomeContainers/OSSection/assets/live.svg'
import Raspberry from 'containers/HomeContainers/OSSection/assets/Raspberry.svg'
import Security from 'containers/HomeContainers/OSSection/assets/Security.svg'
import Virtual from 'containers/HomeContainers/OSSection/assets/virtual.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    stepContent: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      width: '100%',
      marginTop: theme.spacing(4)
    },
    buttonContainer: {
      marginTop: theme.spacing(4),
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%'
    },
    selectedCard: {
      borderColor: theme.palette.primary.main,
      borderWidth: 2,
      borderStyle: 'solid'
    },
    coreIcon: {
      background: 'linear-gradient(180deg, #020024 0%, #030329 55%, #000013 100%)'
    },
    liveIcon: {
      background: 'linear-gradient(99.16deg, #C6FFDD 24.01%, #FBD786 81.75%)'
    },
    virtualIcon: {
      background: 'linear-gradient(99.16deg, #2193b0 24.01%, #6dd5ed 81.75%)'
    },
    iotIcon: {
      background: 'linear-gradient(99.16deg, #FF7E5F 24.01%, #FEB47B 81.75%)'
    },
    homeIcon: {
      background: 'linear-gradient(153.43deg, #00B2FF 16.67%, #0028FF 100%)'
    },
    securityIcon: {
      background: 'linear-gradient(153.43deg, #FF9800 16.67%, #EC4F00 100%)'
    },
    cloudIcon: {
      background: 'linear-gradient(180deg, #E806FF 10%, #B505CC 90%)'
    },
    architectIcon: {
      background: 'linear-gradient(180deg, #B0B0B0 18%, #999999 91%)'
    },
    raspberryIcon: {
      background: 'linear-gradient(90deg, #960E32 58%, #BD0D3B 99%)'
    },
    hacktheboxIcon: {
      background: 'linear-gradient(180deg, #9FEF00 10%, #98D521 90%)'
    },
    desktopEnvironment: {
      marginTop: theme.spacing(8)
    },
    docLink: {
      zIndex: 1,
      position: 'relative'
    },
    headingSubTitle: {
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(5)
    }
  })
)

const DownloadPage = () => {
  const classes = useStyles()
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'))

  const [activeStep, setActiveStep] = useState(0)
  const [selection, setSelection] = useState<{
    category?: string
    edition?: string
    architecture?: string
    raspberryEdition?: string
  }>({})

  const steps = [
    'Choose Category',
    selection.category === 'iot' ? 'Choose Board' : 'Choose Edition',
    selection.category === 'iot' && selection.edition === 'raspberry'
      ? 'Choose Edition'
      : 'Choose Architecture'
  ]

  // const handleNext = () => setActiveStep(prevActiveStep => prevActiveStep + 1)
  const handleBack = () => setActiveStep(prevActiveStep => prevActiveStep - 1)
  const handleReset = () => {
    setSelection({})
    setActiveStep(0)
  }

  const handleSelection = (field: string, value: string) => {
    setSelection(prev => ({ ...prev, [field]: value }))
    setActiveStep(prevActiveStep => {
      const nextStep = prevActiveStep + 1
      return nextStep < steps.length ? nextStep : steps.length
    })
  }

  {
    /*
  const isNextDisabled = () => {
    switch (activeStep) {
      case 0:
        return !selection.category
      case 1:
        return !(
          selection.edition ||
          (selection.category === 'iot' && selection.edition === 'raspberry')
        )
      case 2:
        if (selection.category === 'iot' && selection.edition === 'raspberry') {
          return !selection.raspberryEdition
        }
        return !selection.architecture
      default:
        return false
    }
  }
  */
  }

  const getStepContent = (step: number) => {
    switch (step) {
      case 0:
        return (
          <Stack spacing={2}>
            <Stack direction={isMobile ? 'column' : 'row'} spacing={2}>
              <OSCard
                Icon={Live}
                iconClassName={classes.liveIcon}
                title="Live"
                onClick={() => handleSelection('category', 'live')}
                selected={selection.category === 'live'}
              >
                Full version of the operating system that can be run from a removable storage device
                without installation. This edition allows you to install Parrot on your computer.
              </OSCard>
              <OSCard
                Icon={Virtual}
                iconClassName={classes.virtualIcon}
                title="Virtual"
                onClick={() => handleSelection('category', 'virtual')}
                selected={selection.category === 'virtual'}
              >
                Optimized for running in virtual machines, perfect for virtualized environments.
                VirtualBox, VMware and UTM compatible.
              </OSCard>
              <OSCard
                Icon={IoT}
                title="IoT"
                iconClassName={classes.iotIcon}
                onClick={() => handleSelection('category', 'iot')}
                selected={selection.category === 'iot'}
              >
                Designed for embedded devices and IoT platforms such as Raspberry Pi. More boards
                coming in the future.
              </OSCard>
            </Stack>
            <Stack direction={isMobile ? 'column' : 'row'} spacing={2}>
              {/*
              <OSCard
                Icon={Architect}
                iconClassName={classes.architectIcon}
                title="Architect"
                link="https://gitlab.com/parrotsec/project/installer-script"
                isLinkCard
                buttonText="Download"
              >
                Experience the freedom to install any tool you want when installing Parrot. Security
                tools, multiple desktop enviroments or whatever you need.
              </OSCard>
              */}
            </Stack>
          </Stack>
        )
      case 1:
        if (selection.category === 'live' || selection.category === 'virtual') {
          return (
            <Stack direction={isMobile ? 'column' : 'row'} spacing={2}>
              <OSCard
                Icon={Security}
                iconClassName={classes.securityIcon}
                title="Security"
                onClick={() => handleSelection('edition', 'security')}
                selected={selection.edition === 'security'}
              >
                This is a special purpose operating system designed for Penetration Test and Red
                Team operations. It contains a full arsenal of ready to use pentesting tools.
              </OSCard>
              <OSCard
                Icon={Home}
                title="Home"
                iconClassName={classes.homeIcon}
                onClick={() => handleSelection('edition', 'home')}
                selected={selection.edition === 'home'}
              >
                This edition is a general purpose operating system with the typical Parrot look and
                feel. It is designed for daily use, privacy and software development.
              </OSCard>
              {selection.category !== 'virtual' && (
                <OSCard
                  Icon={HTB}
                  title="HTB"
                  iconClassName={classes.hacktheboxIcon}
                  onClick={() => handleSelection('edition', 'htb')}
                  selected={selection.edition === 'htb'}
                >
                  Unleash the full power of your Pwnbox, a customized hacking cloud box based on
                  ParrotOS Security Edition.
                </OSCard>
              )}
            </Stack>
          )
        }
        if (selection.category === 'iot') {
          return (
            <Stack direction={isMobile ? 'column' : 'row'} spacing={4}>
              <OSCard
                Icon={Raspberry}
                title="Raspberry Pi"
                iconClassName={classes.raspberryIcon}
                onClick={() => handleSelection('edition', 'raspberry')}
                selected={selection.edition === 'raspberry'}
              >
                Optimized edition for Raspberry Pi 3, 4/400 and 5. Raspberry Pi Imager compatible.
              </OSCard>
              <OSCard
                Icon={DeveloperBoardIcon}
                title="...more boards coming in the next months"
              ></OSCard>
            </Stack>
          )
        }
        return null
      case 2:
        if (selection.category === 'iot' && selection.edition === 'raspberry') {
          return (
            <Stack direction={isMobile ? 'column' : 'row'} spacing={4}>
              <OSCard
                Icon={MemoryIcon}
                title="Core"
                iconClassName={classes.coreIcon}
                onClick={() => handleSelection('raspberryEdition', 'core')}
                selected={selection.raspberryEdition === 'core'}
              >
                Install whatever you like with this edition. Only the base packages are installed in
                this edition.
              </OSCard>
              <OSCard
                Icon={Home}
                title="Home"
                iconClassName={classes.homeIcon}
                onClick={() => handleSelection('raspberryEdition', 'home')}
                selected={selection.raspberryEdition === 'home'}
              >
                This version of Parrot is a lightweight installation which provides the essential
                tools needed to start working.
              </OSCard>
              <OSCard
                Icon={Security}
                title="Security"
                iconClassName={classes.securityIcon}
                onClick={() => handleSelection('raspberryEdition', 'security')}
                selected={selection.raspberryEdition === 'security'}
              >
                Full edition. After the installation you have a complete out of the box pentesting
                workstation loaded with a large variety of tools ready to use.
              </OSCard>
            </Stack>
          )
        }
        if (selection.category === 'live') {
          return (
            <Stack direction={isMobile ? 'column' : 'row'} spacing={4}>
              <OSCard
                title="AMD64"
                onClick={() => handleSelection('architecture', 'amd64')}
                selected={selection.architecture === 'amd64'}
              >
                For AMD64 systems. providing support for advanced computing capabilities, enhanced
                performance, and improved compatibility with modern software applications.
                Compatible with both contemporary and legacy computer systems and servers.
              </OSCard>
            </Stack>
          )
        } else {
          return (
            <Stack direction={isMobile ? 'column' : 'row'} spacing={4}>
              <OSCard
                title="AMD64"
                onClick={() => handleSelection('architecture', 'amd64')}
                selected={selection.architecture === 'amd64'}
              >
                For AMD64 systems. providing support for advanced computing capabilities, enhanced
                performance, and improved compatibility with modern software applications.
                Compatible with both contemporary and legacy computer systems and servers.
              </OSCard>
              <OSCard
                title="ARM64"
                onClick={() => handleSelection('architecture', 'arm64')}
                selected={selection.architecture === 'arm64'}
              >
                For ARM64 devices. Designed to support the advanced capabilities of ARM-based
                processors, making it ideal for embedded systems and other applications where power
                consumption and heat dissipation are critical factors.
              </OSCard>
            </Stack>
          )
        }
      default:
        return 'Unknown step'
    }
  }

  {
    /*
  const getFinalLink = () => {
    const { category, edition, architecture, raspberryEdition } = selection
    let link = ''

    if (category === 'live') {
      link = `Parrot-${edition}-6.2_${architecture}.iso`
    } else if (category === 'virtual') {
      link =
        architecture === 'arm64'
          ? `Parrot-${edition}-6.2_${architecture}.utm.zip`
          : `Parrot-${edition}-6.2_${architecture}.ova`
    } else if (category === 'iot' && edition === 'raspberry') {
      link = `Parrot-${raspberryEdition}-6.2_rpi.img.xz`
    }

    return link
  }
    */
  }
  // console.log(getFinalLink())

  type FixedLengthArray<T, L extends number, TObj = [T, ...Array<T>]> = Pick<
    TObj,
    Exclude<keyof TObj, 'splice' | 'push' | 'pop' | 'shift' | 'unshift'>
  > & {
    readonly length: L
    [I: number]: T
    [Symbol.iterator]: () => IterableIterator<T>
  }

  const capitalizeFirstLetter = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const getEditionName = (edition: string) => {
    if (!edition) {
      return ''
    }
    if (edition.toLowerCase() === 'htb') {
      return 'Hack The Box'
    }
    return capitalizeFirstLetter(edition)
  }

  /* eslint-disable  @typescript-eslint/no-explicit-any */
  const getDesktopEnvironmentProps = (selection: any) => {
    let description: React.ReactNode
    let features: {
      hero: string
      content: FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
    }[]

    let downloadOption: {
      iso?: string
      virtualbox?: string
      utm?: string
      vmdk?: string
      img?: string
      qcow2?: string
    } = {}

    let torrent: {
      iso?: string
      virtualbox?: string
      utm?: string
      vmdk?: string
      img?: string
      qcow2?: string
    } = {}

    let screenshots: StaticImageData[] = []
    let raspberryCredentials: boolean = false

    if (selection.category === 'virtual' && selection.edition === 'security') {
      screenshots = [
        parrotSecurity1,
        parrotSecurity2,
        parrotSecurity3,
        parrotSecurity4,
        parrotSecurity5
      ]
      description = (
        <>
          It contains a complete arsenal of ready-to-use pentesting tools. These tools range of
          functionality from reconnaissance and vulnerability scanning to exploitation and
          post-exploitation scanning to exploitation and post-exploitation activities. By leveraging
          the capabilities of this specialized operating system, you can perform comprehensive
          network security posture assessments, pinpoint potential vulnerabilities, and simulate
          real-world cyber attacks.
        </>
      )
      features = [
        {
          hero: 'For Virtual Machines',
          content: [
            {
              heading: 'Pre-configured Environment',
              description: <>All tools are pre-installed and ready to use.</>
            },
            {
              heading: 'VirtualBox, VMware and UTM',
              description: <>Works seamlessly on various virtual machine platforms.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Pentest ready',
          content: [
            {
              heading: 'Tools',
              description: (
                <>
                  Lots of penetration testing tools, all already installed, including BurpSuite,
                  Powersploit, Scapy, Rizin and many more!
                </>
              )
            },
            {
              heading: 'Development',
              description: <>VSCodium and Geany. You can start developing what you want.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Privacy',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Cryptography',
              description: (
                <>
                  Full disk encryption and all encryption tools including zulucrypt, sirikali... at
                  your fingertips.
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz`
        }
        torrent = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova.torrent`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz.torrent`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        downloadOption = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz`
        }
        torrent = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip.torrent`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz.torrent`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz.torrent`
        }
      }
    } else if (selection.category === 'virtual' && selection.edition === 'home') {
      screenshots = [parrotHome1, parrotHome2, parrotHome3, parrotHome4, parrotHome5]
      description = (
        <>
          This edition embodies a versatile operating system with the characteristic ParrotSec
          aesthetics and usability. Tailored for everyday computing tasks, privacy-conscious use and
          software and software development, it meets a wide range of user needs. While prioritizing
          usability and accessibility, it also allows users to customize their experience by
          manually integrating Parrot tools. This allows the creation of a personalized and
          streamlined pentesting environment, ensuring flexibility and efficiency in cybersecurity
          assessments and operations.
        </>
      )
      features = [
        {
          hero: 'For Virtual Machines',
          content: [
            {
              heading: 'Pre-configured Environment',
              description: <>All basic tools are ready to use.</>
            },
            {
              heading: 'VirtualBox, VMware and UTM',
              description: <>Works seamlessly on various virtual machine platforms.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Workstation',
          content: [
            {
              heading: 'Full Office Suite',
              description: (
                <>
                  Pre-installed LibreOffice, and possibility to install other softwares via the
                  Synaptic package manager.
                </>
              )
            },
            {
              heading: 'Multimedia Production',
              description: (
                <>
                  VLC, GIMP and a whole repository from which to install other software such as OBS,
                  Blender, Kdenlive, Krita and more!
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Privacy',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Cryptography',
              description: (
                <>
                  Full disk encryption and all encryption tools including zulucrypt, sirikali... at
                  your fingertips.
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz`
        }
        torrent = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova.torrent`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz.torrent`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        downloadOption = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz`
        }
        torrent = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip.torrent`,
          vmdk: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.vmdk.xz.torrent`
          // qcow2: `https://deb.parrot.sh/parrot/iso/6.2/Parrot-${selection.edition}-6.2_${selection.architecture}.qcow2.xz.torrent`
        }
      }
    } else if (selection.category === 'iot' && selection.edition === 'raspberry') {
      screenshots = [rpi1, rpi2]
      raspberryCredentials = true
      description = (
        <>
          This edition, tailored for IoT devices, is optimized for the Raspberry Pi board, providing
          a lightweight environment with essential Parrot tools pre-installed. This specialized
          edition ensures seamless functionality specifically designed for Raspberry Pi, empowering
          diverse IoT applications and scenarios. To proceed with the installation, read
          <Link
            className={classes.docLink}
            href="https://parrotsec.org/docs/installation/raspberrypi"
          >
            <Typography sx={{ textDecoration: 'underline', fontSize: 18 }}>
              our documentation
            </Typography>
          </Link>
        </>
      )
      features = [
        {
          hero: 'IoT Devices',
          content: [
            {
              heading: 'Lightweight',
              description: <>Optimized for Raspberry Pi 3, 4/400 and 5.</>
            },
            {
              heading: 'Essential Tools',
              description: (
                <>Includes essential tools/libraries/frameworks for development and more.</>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Lots of edition',
          content: [
            {
              heading: 'Core, Home and Security Edition',
              description: <>Available in different editions, each with typical ParrotOS look.</>
            },
            {
              heading: 'Customizable',
              description: (
                <>
                  Install the Home edition and upgrade to the Security edition with a simple
                  command: sudo apt install parrot-tools-full
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Ready for any context',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Pentesting tools',
              description: (
                <>
                  All already installed and preconfigured, including BurpSuite, Powersploit, Scapy,
                  Rizin and many more!
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      downloadOption = {
        img: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.raspberryEdition}-6.3.2_rpi.img.xz`
      }
      torrent = {
        img: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.raspberryEdition}-6.3.2_rpi.img.xz.torrent`
      }
    } else if (selection.category === 'live' && selection.edition === 'home') {
      screenshots = [parrotHome1, parrotHome2, parrotHome3, parrotHome4, parrotHome5]
      description = (
        <>
          This edition embodies a versatile operating system with the characteristic ParrotSec
          aesthetics and usability. Tailored for everyday computing tasks, privacy-conscious use and
          software and software development, it meets a wide range of user needs. While prioritizing
          usability and accessibility, it also allows users to customize their experience by
          manually integrating Parrot tools. This allows the creation of a personalized and
          streamlined pentesting environment, ensuring flexibility and efficiency in cybersecurity
          in cybersecurity assessments and operations.
        </>
      )
      features = [
        {
          hero: 'Live Environment',
          content: [
            {
              heading: 'Pre-configured tools',
              description: <>All basic tools are ready to use.</>
            },
            {
              heading: 'Easy installation',
              description: <>Follow a customized installation by using Calamares.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Workstation',
          content: [
            {
              heading: 'Full Office Suite',
              description: (
                <>
                  Pre-installed LibreOffice, and possibility to install other softwares via the
                  Synaptic package manager.
                </>
              )
            },
            {
              heading: 'Multimedia Production',
              description: (
                <>
                  VLC, GIMP and a whole repository from which to install other software such as OBS,
                  Blender, Kdenlive, Krita and more!
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Privacy',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Cryptography',
              description: (
                <>
                  Full disk encryption and all encryption tools including zulucrypt, sirikali... at
                  your fingertips.
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso`
        }
        torrent = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        downloadOption = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip`
        }
        torrent = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip.torrent`
        }
      }
    } else if (selection.category === 'live' && selection.edition === 'security') {
      screenshots = [
        parrotSecurity1,
        parrotSecurity2,
        parrotSecurity3,
        parrotSecurity4,
        parrotSecurity5
      ]
      description = (
        <>
          It contains a complete arsenal of ready-to-use pentesting tools. These tools range of
          functionality from reconnaissance and vulnerability scanning to exploitation and
          post-exploitation scanning to exploitation and post-exploitation activities. By leveraging
          the capabilities of this specialized operating system, you can perform comprehensive
          network security posture assessments, pinpoint potential vulnerabilities, and simulate
          real-world cyber attacks.
        </>
      )
      features = [
        {
          hero: 'Live Environment',
          content: [
            {
              heading: 'Pre-configured tools',
              description: <>All basic tools are ready to use.</>
            },
            {
              heading: 'Easy installation',
              description: <>Follow a customized installation by using Calamares.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Pentest ready',
          content: [
            {
              heading: 'Tools',
              description: (
                <>
                  Lots of penetration testing tools, all already installed, including BurpSuite,
                  Powersploit, Scapy, Rizin and many more!
                </>
              )
            },
            {
              heading: 'Development',
              description: <>VSCodium and Geany. You can start developing what you want.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Privacy',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Cryptography',
              description: (
                <>
                  Full disk encryption and all encryption tools including zulucrypt, sirikali... at
                  your fingertips.
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso`
        }
        torrent = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        downloadOption = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip`
        }
        torrent = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip.torrent`
        }
      }
    } else if (selection.category === 'live' && selection.edition === 'htb') {
      screenshots = [htb1, htb2, htb3, htb4, htb5]
      description = (
        <>
          Unleash the full power of your Pwnbox, a custom hacking cloud box built on the robust
          framework of ParrotOS Security Edition, right on your personal computer. Whether you
          choose local deployment or virtual exploration, the Pwnbox is ready to facilitate your
          cybersecurity journey with its comprehensive suite of tools and resources. Dive into its
          capabilities to sharpen your skills, explore new techniques, and strengthen your
          proficiency in the ever-evolving landscape of cybersecurity.
        </>
      )
      features = [
        {
          hero: 'Live Environment',
          content: [
            {
              heading: 'Pre-configured tools',
              description: <>All basic tools are ready to use.</>
            },
            {
              heading: 'Easy installation',
              description: <>Follow a customized installation by using Calamares.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Pentest ready',
          content: [
            {
              heading: 'Tools',
              description: (
                <>
                  Lots of penetration testing tools, all already installed, including BurpSuite,
                  Powersploit, Scapy, Rizin and many more!
                </>
              )
            },
            {
              heading: 'Development',
              description: <>VSCodium and Geany. You can start developing what you want.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Hack The Box',
          content: [
            {
              heading: 'Keep practicing',
              description: <>Use ParrotOS during your Hack The Box challenges.</>
            },
            {
              heading: 'Offline Pwnbox',
              description: <>Download and install Pwnbox on your computer.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso`
        }
        torrent = {
          iso: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.iso.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        description = <>Available only for amd64 architecture.</>
      }
    } else if (selection.category === 'virtual' && selection.edition === 'htb') {
      screenshots = [htb1, htb2, htb3, htb4, htb5]
      description = (
        <>
          Unleash the full power of your Pwnbox, a custom hacking cloud box built on the robust
          framework of ParrotOS Security Edition, right on your personal computer. Whether you
          choose local deployment or virtual exploration, the Pwnbox is ready to facilitate your
          cybersecurity journey with its comprehensive suite of tools and resources. Dive into its
          capabilities to sharpen your skills, explore new techniques, and strengthen your
          proficiency in the ever-evolving landscape of cybersecurity.
        </>
      )
      features = [
        {
          hero: 'For Virtual Machines',
          content: [
            {
              heading: 'Pre-configured Environment',
              description: <>All tools are pre-installed and ready to use.</>
            },
            {
              heading: 'VirtualBox, VMware and UTM',
              description: <>Works seamlessly on various virtual machine platforms.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Pentest ready',
          content: [
            {
              heading: 'Tools',
              description: (
                <>
                  Lots of penetration testing tools, all already installed, including BurpSuite,
                  Powersploit, Scapy, Rizin and many more!
                </>
              )
            },
            {
              heading: 'Development',
              description: <>VSCodium and Geany. You can start developing what you want.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        },
        {
          hero: 'Privacy',
          content: [
            {
              heading: 'Anonymity tools',
              description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
            },
            {
              heading: 'Cryptography',
              description: (
                <>
                  Full disk encryption and all encryption tools including zulucrypt, sirikali... at
                  your fingertips.
                </>
              )
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      if (selection.architecture === 'amd64') {
        downloadOption = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova`
        }
        torrent = {
          virtualbox: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.ova.torrent`
        }
      } else if (selection.architecture === 'arm64') {
        downloadOption = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip`
        }
        torrent = {
          utm: `https://deb.parrot.sh/parrot/iso/6.3.2/Parrot-${selection.edition}-6.3.2_${selection.architecture}.utm.zip.torrent`
        }
      }
    } else {
      description = <>Default description.</>
      features = [
        {
          hero: 'Default Feature',
          content: [
            {
              heading: 'Default Heading',
              description: <>Default description.</>
            },
            {
              heading: 'Another Default Heading',
              description: <>Default description.</>
            }
          ] as FixedLengthArray<{ heading: string; description: React.ReactNode }, 2>
        }
      ]
      downloadOption = {}
      torrent = {}
    }

    const editionName = getEditionName(selection.edition)

    return {
      name:
        selection.category === 'iot' && selection.edition === 'raspberry'
          ? 'Raspberry Pi Image'
          : `${editionName} Edition`,
      description,
      version: '6.3 Lorikeet',
      releaseDate: 'Jan 31, 2025',
      architecture:
        selection.category === 'iot' && selection.edition === 'raspberry'
          ? 'arm64'
          : selection.architecture || selection.raspberryEdition || '',
      screenshots,
      requirements: [
        {
          heading: 'Processor',
          description:
            selection.category === 'iot' && selection.edition === 'raspberry'
              ? 'ARM'
              : 'Dual Core CPU'
        },
        { heading: 'Graphics', description: 'No Graphical Acceleration Required' },
        {
          heading: 'Memory',
          description:
            selection.category === 'iot' && selection.edition === 'raspberry'
              ? '512 MB RAM'
              : '1 GB RAM'
        },
        {
          heading: 'Storage',
          description:
            selection.category === 'iot' && selection.edition === 'raspberry' ? '4 GB' : '16 GB'
        }
      ] as FixedLengthArray<{ heading: string; description: string }, 4>,
      features,
      raspberryCredentials,
      downloadOption,
      torrent,
      allHashes: { url: 'https://deb.parrot.sh/parrot/iso/6.3.2/signed-hashes.txt' }
    }
  }

  const desktopEnvironmentProps = getDesktopEnvironmentProps(selection)

  return (
    <>
      <Grid container justifyContent="center" sx={{ marginY: 10 }}>
        <Grid item xs={12} md={9}>
          <Stepper activeStep={activeStep}>
            {steps.map((label, index) => (
              <Step key={index}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <div className={classes.stepContent}>
            {activeStep === steps.length ? (
              <div>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  <Typography variant="h5" sx={{ textAlign: 'center' }}>
                    All steps completed. Enjoy ParrotOS!
                  </Typography>
                  <Typography variant="subtitle2Semi" sx={{ mt: 2 }}>
                    Looking for a different flavor?
                  </Typography>
                  <PButton
                    variant="contained"
                    onClick={handleReset}
                    sx={{ mt: 2, mb: 4, borderRadius: 85, textTransform: 'none', minWidth: 165 }}
                  >
                    Reset
                  </PButton>
                </div>
                <DESection {...desktopEnvironmentProps} />
              </div>
            ) : (
              <div>
                {getStepContent(activeStep)}
                {activeStep > 0 && (
                  <div className={classes.buttonContainer}>
                    <PButton onClick={handleBack} startIcon={<ArrowBack />}>
                      Back
                    </PButton>
                  </div>
                )}
              </div>
            )}
          </div>
        </Grid>
      </Grid>
    </>
  )
}

export default DownloadPage
