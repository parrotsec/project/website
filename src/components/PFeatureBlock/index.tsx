import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import { Paper, PaperProps, Typography, Button } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { ElementType, ReactNode } from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: 24,
    padding: 32,
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  icon: {
    fill: theme.palette.mode === 'light' ? '#FFFFFF' : '#06043E',
    width: 32,
    height: 32
  },
  iconWrapper: {
    width: 64,
    height: 64,
    padding: 16,
    background: theme.palette.mode === 'light' ? '#03232E' : '#FFFFFF',
    borderRadius: 6,
    marginBottom: 20
  },
  button: {
    textTransform: 'none',
    color: theme.palette.mode === 'light' ? '#03232E' : '#05EEFF',
    paddingLeft: 16,
    paddingRight: 16,
    marginLeft: -16,
    borderRadius: 10,
    marginTop: 'auto'
  },
  body: {
    display: 'block',
    marginTop: 20,
    marginBottom: 24
  },
  arrow: {
    fill: theme.palette.mode === 'light' ? '#03232E' : '#05EEFF',
    marginLeft: 8
  },
  fullHeight: {
    height: '100%'
  }
}))

type PFeatureBlockProps = {
  Icon: ElementType
  title: string
  outLink?: boolean
  buttonLink?: string
} & (
  | {
      CustomFooter: ReactNode
      buttonText?: never
    }
  | {
      CustomFooter?: never
      buttonText?: string
    }
) &
  PaperProps

const PFeatureBlock = ({
  children,
  Icon,
  title,
  buttonText,
  buttonLink,
  CustomFooter,
  outLink,
  ...rest
}: PFeatureBlockProps) => {
  const classes = useStyles()

  const handleClick = () => {
    if (buttonLink) {
      if (outLink) {
        window.open(buttonLink, '_blank', 'noopener,noreferrer')
      } else {
        window.location.href = buttonLink
      }
    }
  }

  return (
    <Paper className={classes.root} elevation={0} {...rest}>
      <div className={classes.iconWrapper}>
        <Icon className={classes.icon} />
      </div>
      <Typography variant="h5">{title}</Typography>
      <Typography className={classes.body} variant="body1Semi">
        {children}
      </Typography>
      {CustomFooter ??
        (!buttonText ? null : (
          <Button onClick={handleClick} className={classes.button}>
            {buttonText}
            <ArrowForwardIcon className={classes.arrow} />
          </Button>
        ))}
    </Paper>
  )
}

export default PFeatureBlock
