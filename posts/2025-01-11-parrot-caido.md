---
title: "Parrot + Caido"
date: 2025-01-17T14:36:22+02:00
author: sh4rk
image: /assets/blog/caido/caido.png
description: "We’ve partnered with Caido, a lightweight web security auditing toolkit."
---

# Parrot 🤝 Caido

We’re excited to share some big news: we’ve partnered with Caido, a lightweight web security auditing toolkit.

This collaboration is about bringing the best of both worlds to cybersecurity professionals, combining the versatility of Parrot Security OS with the powerful tool that Caido offers.

We’re always looking for ways to make life easier for penetration testers, ethical hackers, and security teams. Partnering with Caido felt like a natural fit. It’s a platform we’ve come to respect for its innovation and reliability, and we believe our users will love what it brings to the table.

![Caido Overview](/assets/blog/caido/caido_overview.png)

## What makes Caido special?

- **Easy automation**: Caido provides you with a powerful low-code / no-code workflow system to quickly build automations for all your repetitive tasks. From simple drag/drop to a full Javascript VM, you can build everything you need.
- **Keeping you organized**: We know how easy it is to get lost in information. That is why we built easy & instant project change, collections, and many other features to keep things tidy during your work.
- **Client / Server architecture**: Caido is built on modern web technologies with a Rust backend and Vue frontend. It lets you deploy your proxy anywhere and access it remotely with just a browser!

![Caido Overview](/assets/blog/caido/caido.gif)

## How to install

In our next release, it will be included by default. However, if you want to install it on your system now, open a terminal window, update your local repository, and then install Caido by typing

```
$ sudo apt update
$ sudo apt install caido
```
&nbsp;

## What does this partnership bring to you?

One of the most exciting aspects of this partnership is that Caido will now come pre-installed and fully optimized in Parrot Security OS. This means you can start leveraging its powerful web application security testing tools right out of the box, without the hassle of manual installation or setup. By integrating Caido directly into the OS, we’re offering an enhanced user experience tailored to the needs of cybersecurity professionals.

We’re excited for what’s ahead and can’t wait to see how our users will leverage this collaboration to tackle today’s toughest cybersecurity challenges. Stay tuned for more updates, we’re just getting started 🚀
