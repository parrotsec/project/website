---
title: "Parrot 6.3 Release Notes"
date: 2025-01-31T08:10:27+02:00
author: danterolle
image: /assets/blog/6.3/parrot-6.3.png
description: "Parrot OS 6.3 is available for download."
---

The latest version of ParrotOS is here, packed with enhancements to make your system faster, more stable, and even more secure. Parrot 6.3 is tailored to elevate your experience with new features, updated tools, and improved system performance.

#### How do I get ParrotOS?

You can download ParrotOS by clicking [here](https://www.parrotsec.org/download/).

**Important**: Always use official download links to ensure your security and avoid compromised third-party sources.

If you experience issues with direct downloads, don’t worry—we provide Torrent files as an alternative, ideal for bypassing firewalls or network restrictions.

#### How do I upgrade from a previous version?

You can upgrade an existing system via APT using one of the following commands:

*   `sudo parrot-upgrade`

or

*   `sudo apt update && sudo apt full-upgrade`

If you have GPG keyring issues, remember to update it by following [this article](https://parrotsec.org/blog/2025-01-11-parrot-gpg-keys).

**Pro Tip**: While upgrading is supported, we strongly recommend backing up your data and performing a fresh installation for the smoothest experience. This is especially crucial if you’re upgrading from an older version, as it ensures optimal performance and compatibility.

## What's new in ParrotOS 6.3

There are several packages in the ParrotOS system that are upgradeable to newer versions. Some examples include:

- airgeddon **11.40**
- netexec **1.3.0**
- Linux kernel **6.11.5**
- maltego **4.8.1**
- metasploit **6.4.43**
- sqlmap **1.8.12**
- ZAP **2.15.0**
- sherlock **0.15.0**
- Seclists **2024.4**
- enum4linux **1.3.4**
- bloodhound **1.7.2**
- theharvester **4.6.0**
- burpsuite **2024.10.1.1**
- wireshark **4.0.17**
- [NEW] Caido **0.44.1**
- [NEW] Seclists-lite **2024.4**

...and more!

Updated `parrot-core` and `base-files` with new fixes to correct the `$PATH` environment variables. 
Also fixed an issue that prevented the proper display of the Firefox launcher.

In Parrot 6.2, a bug prevented the proper functioning of virtual images in `.ova` format for VirtualBox. 
This issue has now been fixed.

### Minor updates

#### Raspberry Pi updates

- New Kernel version 6.6.62

#### Website updates

- Newer NextJS version
- Updated old dependencies
- improvements for each components

#### Rocket

- Minimal updates on project stability, code refactoring, and general improvements

#### System libraries and more

* **at-spi2-common** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **at-spi2-core** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **avahi-daemon** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **avahi-utils** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **base-files** `2:6.3+parrot2` [upgradable from: 2:6.2+parrot1]
* **bind9-dnsutils** `1:9.18.33-1~deb12u2` [upgradable from: 1:9.18.28-1~deb12u2]
* **bind9-host** `1:9.18.33-1~deb12u2` [upgradable from: 1:9.18.28-1~deb12u2]
* **bind9-libs** `1:9.18.33-1~deb12u2` [upgradable from: 1:9.18.28-1~deb12u2]
* **bsdextrautils** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **bsdutils** `1:2.38.1-5+deb12u3` [upgradable from: 1:2.38.1-5+deb12u1]
* **btrfs-progs** `6.12-1~bpo12+1` [upgradable from: 6.6.3-1.2~bpo12+1]
* **chromium-common** `132.0.6834.159-1~deb12u1` [upgradable from: 129.0.6668.100-1~deb12u1]
* **chromium-driver** `132.0.6834.159-1~deb12u1` [upgradable from: 129.0.6668.100-1~deb12u1]
* **chromium-sandbox** `132.0.6834.159-1~deb12u1` [upgradable from: 129.0.6668.100-1~deb12u1]
* **chromium** `132.0.6834.159-1~deb12u1` [upgradable from: 129.0.6668.100-1~deb12u1]
* **codium** `1.96.4.25026` [upgradable from: 1.94.2.24286]
* **curl** `8.11.1-1~bpo12+1` [upgradable from: 8.10.1-1~bpo12+1]
* **distro-info-data** `0.58+deb12u3` [upgradable from: 0.58+deb12u2]
* **dns-root-data** `2024071801~deb12u1` [upgradable from: 2024041801~deb12u1]
* **dnsmasq-base** `2.90-4~deb12u1` [upgradable from: 2.89-1]
* **dnsmasq** `2.90-4~deb12u1` [upgradable from: 2.89-1]
* **dnsutils** `1:9.18.33-1~deb12u2` [upgradable from: 1:9.18.28-1~deb12u2]
* **e2fsprogs** `1.47.2~rc1-2~bpo12+2` [upgradable from: 1.47.1~rc2-1~bpo12+1]
* **eject** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **exim4-base** `4.98-3~bpo12+1` [upgradable from: 4.98-1~bpo12+1]
* **exim4-config** `4.98-3~bpo12+1` [upgradable from: 4.98-1~bpo12+1]
* **exim4-daemon-light** `4.98-3~bpo12+1` [upgradable from: 4.98-1~bpo12+1]
* **fdisk** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **firefox-esr** `128.6.0esr-1~deb12u1` [upgradable from: 128.3.1esr-1~deb12u1]
* **fonts-opensymbol** `4:102.12+LibO24.8.4-1~bpo12+1` [upgradable from: 4:102.12+LibO24.8.2-1~bpo12+1]
* **galera-4** `26.4.20-0+deb12u1` [upgradable from: 26.4.18-0+deb12u1]
* **geoclue-2.0** `2.6.0-2+deb12u1` [upgradable from: 2.6.0-2]
* **ghostscript** `10.0.0~dfsg-11+deb12u6` [upgradable from: 10.0.0~dfsg-11+deb12u5]
* **gir1.2-atk-1.0** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **gir1.2-atspi-2.0** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **gir1.2-gtk-3.0** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **gir1.2-javascriptcoregtk-4.0** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **gir1.2-soup-2.4** `2.74.3-1+deb12u1` [upgradable from: 2.74.3-1]
* **gir1.2-webkit2-4.0** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **git-man** `1:2.39.5-0+deb12u2` [upgradable from: 1:2.39.5-0+deb12u1]
* **git** `1:2.39.5-0+deb12u2` [upgradable from: 1:2.39.5-0+deb12u1]
* **golang-1.22-go** `1.22.11-1~bpo12+1` [upgradable from: 1.22.7-1~bpo12+1]
* **golang-1.22-src** `1.22.11-1~bpo12+1` [upgradable from: 1.22.7-1~bpo12+1]
* **gstreamer1.0-gl** `1.22.0-3+deb12u4` [upgradable from: 1.22.0-3+deb12u2]
* **gstreamer1.0-plugins-base** `1.22.0-3+deb12u4` [upgradable from: 1.22.0-3+deb12u2]
* **gstreamer1.0-plugins-good** `1.22.0-5+deb12u2` [upgradable from: 1.22.0-5+deb12u1]
* **gstreamer1.0-pulseaudio** `1.22.0-5+deb12u2` [upgradable from: 1.22.0-5+deb12u1]
* **gstreamer1.0-x** `1.22.0-3+deb12u4` [upgradable from: 1.22.0-3+deb12u2]
* **gtk-update-icon-cache** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **inxi** `3.3.37-1-1~bpo12+1` [upgradable from: 3.3.36-1-1~bpo12+1]
* **iproute2** `6.12.0-1~bpo12+1` [upgradable from: 6.9.0-1~bpo12+1]
* **libapache2-mod-php8.2** `8.2.26-1~deb12u1` [upgradable from: 8.2.24-1~deb12u1]
* **libapr1** `1.7.2-3+deb12u1` [upgradable from: 1.7.2-3]
* **libarchive13** `3.6.2-1+deb12u2` [upgradable from: 3.6.2-1+deb12u1]
* **libatk-adaptor** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **libatk-bridge2.0-0** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **libatk1.0-0** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **libatspi2.0-0** `2.55.0.1-1~bpo12+1` [upgradable from: 2.52.0-1~bpo12+1]
* **libavahi-client3** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libavahi-common-data** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libavahi-common3** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libavahi-core7** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libavahi-glib1** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libavahi-ui-gtk3-0** `0.8-10+deb12u1` [upgradable from: 0.8-10]
* **libblkid1** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **libc-bin** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libc-dev-bin** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libc-devtools** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libc-l10n** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libc6-dev** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libc6** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **libcom-err2** `1.47.2~rc1-2~bpo12+2` [upgradable from: 1.47.1~rc2-1~bpo12+1]
* **libcpupower1** `6.11.5-1parrot1` [upgradable from: 6.10.11-1~bpo12+1]
* **libcurl3-gnutls** `8.11.1-1~bpo12+1` [upgradable from: 8.10.1-1~bpo12+1]
* **libcurl3-nss** `7.88.1-10+deb12u9` [upgradable from: 7.88.1-10+deb12u7]
* **libcurl4** `8.11.1-1~bpo12+1` [upgradable from: 8.10.1-1~bpo12+1]
* **libebml5** `1.4.4-1+deb12u1` [upgradable from: 1.4.4-1]
* **libegl-mesa0** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **libext2fs2** `1.47.2~rc1-2~bpo12+2` [upgradable from: 1.47.1~rc2-1~bpo12+1]
* **libfdisk1** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **libgail-3-0** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **libgbm1** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **libgl1-mesa-dri** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **libglapi-mesa** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **libglib2.0-0** `2.74.6-2+deb12u5` [upgradable from: 2.74.6-2+deb12u3]
* **libglib2.0-bin** `2.74.6-2+deb12u5` [upgradable from: 2.74.6-2+deb12u3]
* **libglib2.0-data** `2.74.6-2+deb12u5` [upgradable from: 2.74.6-2+deb12u3]
* **libglx-mesa0** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **libgs-common** `10.0.0~dfsg-11+deb12u6` [upgradable from: 10.0.0~dfsg-11+deb12u5]
* **libgs10-common** `10.0.0~dfsg-11+deb12u6` [upgradable from: 10.0.0~dfsg-11+deb12u5]
* **libgs10** `10.0.0~dfsg-11+deb12u6` [upgradable from: 10.0.0~dfsg-11+deb12u5]
* **libgstreamer-gl1.0-0** `1.22.0-3+deb12u4` [upgradable from: 1.22.0-3+deb12u2]
* **libgstreamer-plugins-base1.0-0** `1.22.0-3+deb12u4` [upgradable from: 1.22.0-3+deb12u2]
* **libgstreamer1.0-0** `1.22.0-2+deb12u1` [upgradable from: 1.22.0-2]
* **libgtk-3-0** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **libgtk-3-bin** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **libgtk-3-common** `3.24.38-2~deb12u3` [upgradable from: 3.24.38-2~deb12u2]
* **libheif1** `1.15.1-1+deb12u1` [upgradable from: 1.15.1-1]
* **libjavascriptcoregtk-4.0-18** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **libjavascriptcoregtk-4.1-0** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **libkpathsea6** `2022.20220321.62855-5.1+deb12u2` [upgradable from: 2022.20220321.62855-5.1+deb12u1]
* **libldb2** `2:2.10.0+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:2.9.1+samba4.20.5+dfsg-1~bpo12+1]
* **liblouis-data** `3.32.0-1~bpo12+1` [upgradable from: 3.31.0-2~bpo12+1]
* **liblouis20** `3.32.0-1~bpo12+1` [upgradable from: 3.31.0-2~bpo12+1]
* **libmariadb3** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **libmount1** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **libmpg123-0** `1.31.2-1+deb12u1` [upgradable from: 1.31.2-1]
* **libnghttp3-9** `1.6.0-2~bpo12+1` [upgradable from: 1.4.0-1~bpo12+1]
* **libngtcp2-16** `1.9.1-1~bpo12+1` [upgradable from: 1.6.0-1~bpo12+1]
* **libngtcp2-crypto-gnutls8** `1.9.1-1~bpo12+1` [upgradable from: 1.6.0-1~bpo12+1]
* **libnss3** `2:3.87.1-1+deb12u1` [upgradable from: 2:3.87.1-1]
* **libntfs-3g89** `1:2022.10.3-1+deb12u2` [upgradable from: 1:2022.10.3-1+deb12u1]
* **libopenjp2-7** `2.5.0-2+deb12u1` [upgradable from: 2.5.0-2]
* **libout123-0** `1.31.2-1+deb12u1` [upgradable from: 1.31.2-1]
* **libpam-systemd** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **libpipewire-0.3-0** `1.2.7-1~bpo12+1` [upgradable from: 1.2.4-1~bpo12+1]
* **libpipewire-0.3-common** `1.2.7-1~bpo12+1` [upgradable from: 1.2.4-1~bpo12+1]
* **libpq5** `15.10-0+deb12u1` [upgradable from: 15.8-0+deb12u1]
* **libpython3.11-dev** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **libpython3.11-minimal** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **libpython3.11-stdlib** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **libpython3.11** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **librabbitmq4** `0.11.0-1+deb12u1` [upgradable from: 0.11.0-1+b1]
* **libreoffice-base-core** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-calc** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-common** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-core** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-draw** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-impress** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-math** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-style-colibre** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-calc** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-common** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-draw** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-impress** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-math** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-uiconfig-writer** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libreoffice-writer** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libsmartcols1** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **libsmbclient** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **libsoup-gnome2.4-1** `2.74.3-1+deb12u1` [upgradable from: 2.74.3-1]
* **libsoup2.4-1** `2.74.3-1+deb12u1` [upgradable from: 2.74.3-1]
* **libsoup2.4-common** `2.74.3-1+deb12u1` [upgradable from: 2.74.3-1]
* **libspa-0.2-modules** `1.2.7-1~bpo12+1` [upgradable from: 1.2.4-1~bpo12+1]
* **libsqlite3-0** `3.40.1-2+deb12u1` [upgradable from: 3.40.1-2]
* **libsrt1.5-gnutls** `1.5.1-1+deb12u1` [upgradable from: 1.5.1-1]
* **libss2** `1.47.2~rc1-2~bpo12+2` [upgradable from: 1.47.1~rc2-1~bpo12+1]
* **libssl-dev** `3.0.15-1~deb12u1` [upgradable from: 3.0.14-1~deb12u2]
* **libssl3** `3.0.15-1~deb12u1` [upgradable from: 3.0.14-1~deb12u2]
* **libsvn1** `1.14.2-4+deb12u1` [upgradable from: 1.14.2-4+b2]
* **libsyn123-0** `1.31.2-1+deb12u1` [upgradable from: 1.31.2-1]
* **libsynctex2** `2022.20220321.62855-5.1+deb12u2` [upgradable from: 2022.20220321.62855-5.1+deb12u1]
* **libsystemd-shared** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **libsystemd0** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **libtalloc2** `2:2.4.2+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 2.4.2-1~bpo12+1]
* **libtdb1** `2:1.4.12+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 1.4.10-1~bpo12+1]
* **libtevent0** `2:0.16.1+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 0.16.1-2~bpo12+1]
* **libtiff6** `4.5.0-6+deb12u2` [upgradable from: 4.5.0-6+deb12u1]
* **libudev1** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **libuno-cppu3** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libuno-cppuhelpergcc3-3** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libuno-purpenvhelpergcc3-3** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libuno-sal3** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libuno-salhelpergcc3-3** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **libuuid1** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **libwbclient0** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **libwebkit2gtk-4.0-37** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **libwebkit2gtk-4.1-0** `2.46.5-1~deb12u1` [upgradable from: 2.42.1-1~bpo12+1]
* **libwireshark-data** `4.0.17-0+deb12u1` [upgradable from: 4.0.11-1~deb12u1]
* **libwireshark16** `4.0.17-0+deb12u1` [upgradable from: 4.0.11-1~deb12u1]
* **libwiretap13** `4.0.17-0+deb12u1` [upgradable from: 4.0.11-1~deb12u1]
* **libwsutil14** `4.0.17-0+deb12u1` [upgradable from: 4.0.11-1~deb12u1]
* **locales-all** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **locales** `2.36-9+deb12u9` [upgradable from: 2.36-9+deb12u8]
* **logsave** `1.47.2~rc1-2~bpo12+2` [upgradable from: 1.47.1~rc2-1~bpo12+1]
* **mariadb-client-core** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-client** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-common** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-plugin-provider-bzip2** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-plugin-provider-lz4** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-plugin-provider-lzma** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-plugin-provider-lzo** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-plugin-provider-snappy** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-server-core** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mariadb-server** `1:10.11.9-0+deb12u1` [upgradable from: 1:10.11.6-0+deb12u1]
* **mesa-libgallium** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **mesa-va-drivers** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **mesa-vdpau-drivers** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **mesa-vulkan-drivers** `24.2.8-1~bpo12+1` [upgradable from: 24.2.2-1~bpo12+1]
* **metasploit-framework** `6.4.43-0parrot1` [upgradable from: 6.3.54-0parrot1]
* **mount** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **mpg123** `1.31.2-1+deb12u1` [upgradable from: 1.31.2-1]
* **netexec** `1.3.0-0parrot1` [upgradable from: 1.2.0-0parrot4]
* **node-postcss** `8.4.20+~cs8.0.23-1+deb12u1` [upgradable from: 8.4.20+~cs8.0.23-1]
* **ntfs-3g** `1:2022.10.3-1+deb12u2` [upgradable from: 1:2022.10.3-1+deb12u1]
* **openjdk-17-jdk-headless** `17.0.13+11-2~deb12u1` [upgradable from: 17.0.12+7-2~deb12u1]
* **openjdk-17-jdk** `17.0.13+11-2~deb12u1` [upgradable from: 17.0.12+7-2~deb12u1]
* **openjdk-17-jre-headless** `17.0.13+11-2~deb12u1` [upgradable from: 17.0.12+7-2~deb12u1]
* **openjdk-17-jre** `17.0.13+11-2~deb12u1` [upgradable from: 17.0.12+7-2~deb12u1]
* **opensc-pkcs11** `0.23.0-0.3+deb12u2` [upgradable from: 0.23.0-0.3+deb12u1]
* **opensc** `0.23.0-0.3+deb12u2` [upgradable from: 0.23.0-0.3+deb12u1]
* **openssh-client** `1:9.2p1-2+deb12u4` [upgradable from: 1:9.2p1-2+deb12u3]
* **openssh-server** `1:9.2p1-2+deb12u4` [upgradable from: 1:9.2p1-2+deb12u3]
* **openssh-sftp-server** `1:9.2p1-2+deb12u4` [upgradable from: 1:9.2p1-2+deb12u3]
* **openssl** `3.0.15-1~deb12u1` [upgradable from: 3.0.14-1~deb12u2]
* **parrot-apps-basics** `6.3` [upgradable from: 6.2.3]
* **parrot-core-lite** `6.3` [upgradable from: 6.2.3]
* **parrot-core** `6.3` [upgradable from: 6.2.3]
* **parrot-desktop-mate** `6.2.3` [upgradable from: 6.2.2]
* **parrot-displaymanager** `6.2.3` [upgradable from: 6.2.2]
* **parrot-firefox-profiles** `6.2.3` [upgradable from: 6.2.2]
* **parrot-interface-common** `6.2.3` [upgradable from: 6.2.2]
* **parrot-interface-home** `6.2.3` [upgradable from: 6.2.2]
* **parrot-interface** `6.2.3` [upgradable from: 6.2.2]
* **parrot-menu** `2:2024.12.05` [upgradable from: 2:2024.08.06]
* **parrot-meta-crypto** `6.3.0` [upgradable from: 6.2.0]
* **parrot-meta-devel** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-automotive** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-cloud** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-forensics** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-full** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-infogathering** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-maintain** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-password** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-postexploit** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-pwn** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-reporting** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-reversing** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-sniff** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-vuln** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-web** `6.3.0` [upgradable from: 6.2.0]
* **parrot-tools-wireless** `6.3.0` [upgradable from: 6.2.0]
* **php8.2-cli** `8.2.26-1~deb12u1` [upgradable from: 8.2.24-1~deb12u1]
* **php8.2-common** `8.2.26-1~deb12u1` [upgradable from: 8.2.24-1~deb12u1]
* **php8.2-opcache** `8.2.26-1~deb12u1` [upgradable from: 8.2.24-1~deb12u1]
* **php8.2-readline** `8.2.26-1~deb12u1` [upgradable from: 8.2.24-1~deb12u1]
* **postgresql-15** `15.10-0+deb12u1` [upgradable from: 15.8-0+deb12u1]
* **postgresql-client-15** `15.10-0+deb12u1` [upgradable from: 15.8-0+deb12u1]
* **python3-aiohttp** `3.8.4-1+deb12u1` [upgradable from: 3.8.4-1]
* **python3-cryptography** `38.0.4-3+deb12u1` [upgradable from: 38.0.4-3]
* **python3-jinja2** `3.1.2-1+deb12u1` [upgradable from: 3.1.2-1]
* **python3-ldb** `2:2.10.0+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:2.9.1+samba4.20.5+dfsg-1~bpo12+1]
* **python3-pkg-resources** `66.1.1-1+deb12u1` [upgradable from: 66.1.1-1]
* **python3-samba** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **python3-setuptools-whl** `66.1.1-1+deb12u1` [upgradable from: 66.1.1-1]
* **python3-setuptools** `66.1.1-1+deb12u1` [upgradable from: 66.1.1-1]
* **python3-talloc** `2:2.4.2+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 2.4.2-1~bpo12+1]
* **python3-tdb** `2:1.4.12+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 1.4.10-1~bpo12+1]
* **python3-tornado** `6.2.0-3+deb12u1` [upgradable from: 6.2.0-3]
* **python3-uno** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **python3-urllib3** `1.26.12-1+deb12u1` [upgradable from: 1.26.12-1]
* **python3-werkzeug** `2.2.2-3+deb12u1` [upgradable from: 2.2.2-3]
* **python3.11-dev** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **python3.11-minimal** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **python3.11-venv** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **python3.11** `3.11.2-6+deb12u5` [upgradable from: 3.11.2-6+deb12u4]
* **qemu-guest-agent** `1:9.2.0+ds-3~bpo12+2` [upgradable from: 1:9.0.2+ds-1~bpo12+1]
* **qemu-user-static** `1:9.2.0+ds-3~bpo12+2` [upgradable from: 1:9.0.2+ds-1~bpo12+1]
* **rfkill** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **rsync** `3.2.7-1+deb12u2` [upgradable from: 3.2.7-1]
* **samba-common-bin** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **samba-common** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **samba-libs** `2:4.21.3+dfsg-6~bpo12+1` [upgradable from: 2:4.20.5+dfsg-1~bpo12+1]
* **sherlock** `0.15.0-1parrot3` [upgradable from: 0.14.3+git20230421-0parrot1]
* **sqlite3** `3.40.1-2+deb12u1` [upgradable from: 3.40.1-2]
* **sqlmap** `1.8.12-1parrot1` [upgradable from: 1.8.3-1parrot1]
* **subversion** `1.14.2-4+deb12u1` [upgradable from: 1.14.2-4+b2]
* **systemd-dev** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **systemd-sysv** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **systemd** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **tdb-tools** `2:1.4.12+samba4.21.3+dfsg-6~bpo12+1` [upgradable from: 1.4.10-1~bpo12+1]
* **tshark** `4.0.17-0+deb12u1` [upgradable from: 4.0.11-1~deb12u1]
* **tzdata** `2024b-0+deb12u1` [upgradable from: 2024a-0+deb12u1]
* **ucf** `3.0043+nmu1+deb12u1` [upgradable from: 3.0043+nmu1]
* **udev** `254.22-1~bpo12+1` [upgradable from: 254.16-1~bpo12+1]
* **uno-libs-private** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **ure** `4:24.8.4-1~bpo12+1` [upgradable from: 4:24.8.2-1~bpo12+1]
* **util-linux-extra** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **util-linux** `2.38.1-5+deb12u3` [upgradable from: 2.38.1-5+deb12u1]
* **webext-ublock-origin-chromium** `1.60.0+dfsg-1parrot1` [upgradable from: 1.54.0+dfsg-1parrot1]
* **webext-ublock-origin-firefox** `1.60.0+dfsg-1parrot1` [upgradable from: 1.54.0+dfsg-1parrot1]
* **xserver-xorg-core** `2:21.1.7-3+deb12u8` [upgradable from: 2:21.1.7-3+deb12u7]
* **xserver-xorg-legacy** `2:21.1.7-3+deb12u8` [upgradable from: 2:21.1.7-3+deb12u7]
* **yt-dlp** `2025.01.15-1~bpo12+1` [upgradable from: 2024.10.07-1~bpo12+1]