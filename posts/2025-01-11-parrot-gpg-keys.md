---
title: "Parrot rolls out new GPG keys!"
date: 2025-01-11T11:15:48+02:00
author: danterolle
image: /assets/blog/new-gpg-keys/gpg-keys.png
description: "An error occurred during the signature verification. How to fix it?"
---

Some of you have recently encountered the following warnings:

![](/assets/blog/new-gpg-keys/warnings.png)

The reported error is due to the GPG keys used to sign the ParrotOS repositories being expired or invalid. Specifically, the message `EXPKEYSIG 823BF07CEB5C469B` indicates that the GPG key with the specified ID has expired (where EXP stands for "expired").

When using an apt based system (like ParrotOS/Debian) to download or update packages, the system verifies the integrity and authenticity of the repository files using digital signatures provided by the GPG keys.

### What is GPG?

GPG, or GNU Privacy Guard, is a free software for encrypting and signing data and communications. It's an implementation of the OpenPGP standard as defined by [RFC9580](https://datatracker.ietf.org/doc/html/rfc9580), which is how it is used in a variety of secure applications.

This is actually one of the mechanisms we use to ensure the secure installation of packages without external interference. Each package is signed with our GPG key.

## So, how can I solve this problem?

If `apt` notifies you that your GPG key has expired, here is the solution to get things back on track. Download and install the latest version of **parrot-archive-keyring** from [our repository](https://deb.parrot.sh/parrot/pool/main/p/parrot-archive-keyring/parrot-archive-keyring_2024.12_all.deb):

[https://deb.parrot.sh/parrot/pool/main/p/parrot-archive-keyring/parrot-archive-keyring_2024.12_all.deb](https://deb.parrot.sh/parrot/pool/main/p/parrot-archive-keyring/parrot-archive-keyring_2024.12_all.deb)

Once installed, the new key with **ID 7A8286AF0E81EE4A** will replace the expired key **ID 823BF07CEB5C469B**. To ensure everything is correct, the SHA256 hash of the downloaded *.deb* file should be:

```
$ sha256sum ./parrot-archive-keyring_2024.12_all.deb
$ 188c74cf726fad499ec416dc6b7dcd9718800aa7178290c10ef7040aea9b5272
```

## Summarizing

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

APT KEYRING ERROR

in case apt said that your gpg key is expired, 
the solution is to download and install the 
latest version of parrot-archive-keyring from our repository

https://deb.parrot.sh/parrot/pool/main/p/
parrot-archive-keyring/parrot-archive-keyring_2024.12_all.deb

once installed. the new key with id 7A8286AF0E81EE4A 
will replace the expired key with id 823BF07CEB5C469B

once downloaded, the sha256 hash of the deb file should be
188c74cf726fad499ec416dc6b7dcd9718800aa7178290c10ef7040aea9b5272


this message is digitally signed with the team leader's personal gpg key
(f4c6b9a4), with the expired repository key and with the new one

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzB4RdX9xx1hDf3ltLtvDE03BeQAFAmeBSI0ACgkQLtvDE03B
eQBm4w/9HLqPGa6P6c9vyPJXoUNYHXHIY+EgP5AyJnaCSoL7hMzUuawFyy1vA8P/
9+2UsJEfWCpXPItmwzXcWRlAMRZvTfKQSFqNl7Ue6j/ftYZCeJpGKZUb1CDspM6X
UowO5+Cx+1nErcV9Ctc+SLvSmd5w765bYAlRFvV3PqR5MISiytznY5T1s7VTLT8a
t80pAZzSROnpagSztxK8ut2X82rIytoq9GUPTyucu85bif9LQUgKQmNpf9e/WFCl
/Lv+5opOE2irUYxLLFGhVu/LJ5IOYSj0h1X5pULbzsc459yb+X4j+W3jpUwk7lLZ
3yN2wMQyEzM1Qhwpg6GJhXdLaDk3b6xxqIVyqgb0mHv0wPcEGbJDWSW+Ay0aJ0CS
Jk4kruHH6uByISY93s9qr0UsjbafPFFdRgVOTkBq7rnB37FuYOFqJqHtXljvUkON
u6gRzyBrl4kdzzxS2Co3LmFSi4NKh8cf/f1UICaof5uCR05PWMwYiz1p7v7gxCKG
2+KljsSyHovmcalohEVoR1cGtfLbNRm3Lj4pufaXzZ7ENGHWeHVsAwssO5uXitTJ
3PrH8lKoP37JsNJZtuCidX4xcx9QC3qc4kBwDpjS0Two1Ha2uARN8I1fD9+2aj7a
gArtQyqQZA92JZ9a1pF7tqPn7R7RIXOHZKvnZsUTzG1wZGOyHHqJAjMEAQEKAB0W
IQS3EYIjRlUuTZLaAt96goavDoHuSgUCZ4FIjgAKCRB6goavDoHuSqseD/4tQxSr
ia1rnu06cU8iYscfC0VubEuXAKqzftUV2ERmEjbAQu8euVNbzvfsMTD+ufSRzG8J
HZjlw74bhQCA6HqLDMjHygLjMGMdhGw3/p7/u+n3El40TEI/NjDjSYJzHuhlokTb
+eLnFPVJc4kNOxVFdHLhDzfVGiLBcFLriYiX0bdwqp1KUaLD+7R5ZSDd7ijUZX1r
hxXDUk5CHJ7Fcxn0VwgwSh81hWIPzXQ3DIaHxcErsGsKvFqG0eUksrca+xi6UN0m
4O5QMSSTtJtwvJafIXNnxoE1vd+gch/HeUFvTX8pYRccLSyLYwkdvZ4a6v9FOel0
acSSHgMZ8zge3pzSCYC1K++17XWg6utoGGLKiLIez24rS+2un3GY1EgdaNJSM3pq
aTtcBFSZOVWoCE6S2KKJNHFerKXWDXPeQpLX5Q4xqCPGPk6Y/36A3+AFEYQBg2r4
DLgGKTI2ZLDcd0ThOEj5xh/j9OohbQSZlfl49uDjGUqWO04VxnL+52pYTlzlbauw
+jc1+3xH118ZnUP8QDkwiLMoAyA3yEMIOAdKyQKzD/x8ymm0slMLvhv+Y31QE0+j
SW1zN+nprtvFc32y7oD7g2RsPelX8m1Sy8pw43sZHErZ9Zn/wN9T0B0Kuzpi+xp8
ziIg1WcLolkcMOHsGruRd4b+k3kcryCeOg9d2okCMwQBAQoAHRYhBItAYMppqXNW
stz1UYI78HzrXEabBQJngUiOAAoJEII78HzrXEabfpIP/1raezwqyHwyg5CeZFUe
GVT2oIEB6vwzcx45y37QgK5aEmPkU9R1Rob5JcTMi+MxhFHU8Ap7WIo0GleoY9Gk
mE4/ZRxD7yo5LIG05sYJ36VBYcXY3rBXbzIwis2luLXQ9+8zg+2uUDv9VHEp7ciK
U7ny6zebeH1AIlWG9E+r5bx0KzmeoE5w9EUY1Vr7/+hf0K+23UUYZ9vI6GN1o9k1
RBXZ3bYP3XOGPlFXEh6OSd+73mgGN5WAad61argGXDeGZOD7Kzwb0K6idyMsPRrf
aSTHW3ZLj2sOPb+jEEoIIJtb/mXiOg2rzw5swBCYNws31gKolOTfrNtxF1kI0kh1
LaFPWJcHfXaQsdW3kxLa0pnr+IeNrI6Erlgrrrfhrz/7S1Ai2J6FIF2emGyxI9J8
kLT5ozlht5wh8AfGE7LzZS9/GXBvriDv4j7b5TYMgdEFa+PI4NhV+m6vis3tIPXY
nZvODnrO9gygJRRS6JY3XKgoIZPDzMxFZCyJzLZUKzEhRKT9aCSybiIq1HPfCAxL
/FUuYHi20VL7GUaA2PFEWZauPjdArPEOKw9IDJ8sZ+WSWnuyT7iu/jlEc2UAfBQZ
ykZ3qhzX/PoNPxRKQSJj374AE4207+39dIywJs/m1KvrYS/Grv9i5mtN0qS11R3u
+YKCyNGmO2MbT0jW9a50xR1Q
=ZggF
-----END PGP SIGNATURE-----
```