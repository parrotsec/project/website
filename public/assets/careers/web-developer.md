### ✨The core mission of the Web Developer:
We are on the lookout for our next web developer for ParrotSec. It is an open source project (available on GitLab), an operating system derived from Debian Linux. The Web Developer will be taking care of Parrot website and some other services. One of the main responsibilities will be the migration of the website to the new version. For this migration, the input of the new joiner on new technologies will be highly appreciated.

### 🍺 The fellowship you’ll be joining:

The whole team consists not only of colleagues, but also of the community members that play an active role in developing the project. The web developer will be collaborating closely with the other ParrotSec team members and the community. They will be reporting to the Senior member of the team that will be providing support & guidance, while giving space for growth & development.

#### ⚔️ Technology tools & weapons you’ll be using:

Typescript, React, Material UI

### 🚀 The adventures that await you after becoming Web Developer at ParrotSec:

- Writing effective and testable code
- Maintain the ParrotSec website, updating its components and dependencies to keep it up to date
- Create new components and new webpages as needed
- Be able to maintain and possibly expand other web projects related to ParrotSec
- Contribute and maintain technical documentation on GitLab
- Stay up to date on emerging technologies

### 🏆 Skills, knowledge, and experience points required to unlock the role of Web Developer at ParrotSec:

- At least 1-2 years of development experience using JSX/TSX, ReactJS, and NextJS
- Experience with Material UI, TailwindCSS (DaisyUI), Styled Components
- Knowledge of ESLint, Prettier, Babel, Webpack
- Full understanding of Git, GitFlow, conventional commits and semantic versioning
- Excellent ability to adapt to new and different web technologies
- Strong written and spoken English
- Excellent analytical skills
- Soft skills and team player capabilities

#### Bonus points :

- Good understanding of Docker and container technologies
- ViteJS knowledge or other frameworks for building web apps
- GSAP and/or Framer Motion experience
- Database (MariaDB, MongoDB) and Backend (Django or Laravel) experience



